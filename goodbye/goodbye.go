package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Goodbye from Web Server in Go")
	})

	http.HandleFunc("/correct", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Connect is set")
	})

	fmt.Println("Server is listening...")
	http.ListenAndServe("0.0.0.0:2131", nil)
}
