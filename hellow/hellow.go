package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/index.html")
	})
	http.HandleFunc("/run-fail", func(w http.ResponseWriter, r *http.Request) {
		response, error := http.Get("http://localhost:2131/")
		if error != nil {
			fmt.Fprint(w, error)
			return
		}
		defer response.Body.Close()
		for true {

			bs := make([]byte, 1014)
			n, err := response.Body.Read(bs)
			fmt.Fprint(w, string(bs[:n]))
			if n == 0 || err != nil {
				break
			}
		}
	})

	http.HandleFunc("/run-correct", func(w http.ResponseWriter, r *http.Request) {
		response, error := http.Get("http://goodbye:2131/correct")
		if error != nil {
			fmt.Fprint(w, error)
			return
		}
		defer response.Body.Close()
		for true {

			bs := make([]byte, 1014)
			n, err := response.Body.Read(bs)
			fmt.Fprint(w, string(bs[:n]))
			if n == 0 || err != nil {
				break
			}
		}
	})

	fmt.Println("Server is listening...")
	http.ListenAndServe("0.0.0.0:2130", nil)
}
